import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    let emojiElDiv=document.getElementById('emojis');
    this.emojis = emojis;
    emojiElDiv.textContent='';
    let stringEmoji = emojis.join('');
    let pEl = document.createElement('p');
    pEl.textContent=stringEmoji
    emojiElDiv.appendChild(pEl)
  }

  addBananas() {
   let newBananaMonkey=this.emojis.map(x=>{
      return x+this.banana});
       
      this.setEmojis(newBananaMonkey);
       
     
  }
}
